function shouldOverrideMessage(message) {
    const setting = game.settings.get("df-chat-cards", "displaySetting");
    if (setting !== "none") {
        const user = game.users.get(message.user);
        if (user) {
            const isSelf = user._id === game.user._id;
            const isGM = user.isGM;

            if ((setting === "allCards")
                || (setting === "self" && isSelf)
                || (setting === "selfAndGM" && (isSelf || isGM))
                || (setting === "gm" && isGM)
                || (setting === "player" && !isGM)
            ) {
                return true;
            }
        }
    }
    return false;
}

Hooks.once('init', async function () {
    CONFIG.ChatMessage.template = "modules/df-chat-cards/templates/base-chat-message.html";

    game.settings.register("df-chat-cards", "displaySetting", {
        name: "DFCHATCARDS.SETTINGS.displaySetting.name",
        hint: "DFCHATCARDS.SETTINGS.displaySetting.hint",
        scope: "client",
        config: true,
        default: "allCards",
        type: String,
        choices: {
            "allCards": "DFCHATCARDS.SETTINGS.displaySetting.allCards",
            "selfAndGM": "DFCHATCARDS.SETTINGS.displaySetting.selfAndGM",
            "self": "DFCHATCARDS.SETTINGS.displaySetting.self",
            "gm": "DFCHATCARDS.SETTINGS.displaySetting.gm",
            "player": "DFCHATCARDS.SETTINGS.displaySetting.player",
            "none": "DFCHATCARDS.SETTINGS.displaySetting.none",
        }
    });

    game.settings.register("df-chat-cards", "cardStyle", {
        name: "DFCHATCARDS.SETTINGS.cardStyle.name",
        hint: "DFCHATCARDS.SETTINGS.cardStyle.hint",
        scope: "client",
        config: true,
        default: "header",
        type: String,
        choices: {
            "header": "DFCHATCARDS.SETTINGS.cardStyle.header",
            "underline": "DFCHATCARDS.SETTINGS.cardStyle.underline",
            "topBar": "DFCHATCARDS.SETTINGS.cardStyle.topBar",
        }
    });

    game.settings.register("df-chat-cards", "borderOverride", {
        name: "DFCHATCARDS.SETTINGS.borderOverride.name",
        hint: "DFCHATCARDS.SETTINGS.borderOverride.hint",
        scope: "client",
        config: true,
        default: true,
        type: Boolean
    });

    game.settings.register("df-chat-cards", "insertSpeakerImage", {
        name: "DFCHATCARDS.SETTINGS.insertSpeakerImage.name",
        hint: "DFCHATCARDS.SETTINGS.insertSpeakerImage.hint",
        scope: "client",
        config: true,
        default: true,
        type: Boolean
    });
});

Hooks.once("setup", function () {
    Handlebars.registerHelper("getSpeakerImage", function (message) {
        const speaker = message.speaker;
        if (speaker) {
            if (speaker.token) {
                const token = game.scenes.get(speaker.scene)?.tokens?.get(speaker.token);
                if (token) {
                    return token.texture.src;
                }
            }

            if (speaker.actor) {
                const actor = Actors.instance.get(speaker.actor);
                if (actor) {
                    return actor.texture.src;
                }
            }
        }
        
        return "icons/svg/mystery-man.svg";
    });

    Handlebars.registerHelper("showSpeakerImage", function (message) {
        const insertSpeakerImage = game.settings.get("df-chat-cards", "insertSpeakerImage");
        if (!insertSpeakerImage) {
            return false;
        }

        const speaker = message.speaker;
        if (!speaker) {
            return false;
        } else {
            let bHasImage = false;
            if (speaker.token) {
                const token = game.scenes.get(speaker.scene)?.tokens?.get(speaker.token);
                if (token) {
                    bHasImage = bHasImage || token.texture.src != null;
                }
            }

            if (speaker.actor) {
                const actor = Actors.instance.get(speaker.actor);
                if (actor) {
                    bHasImage = bHasImage || actor.texture.src != null;
                }
            }

            if (!bHasImage) {
                return false;
            }
        }

        return shouldOverrideMessage(message);
    });

    Handlebars.registerHelper("useVideoForSpeakerImage", function (message) {
        const speaker = message.speaker;
        if (!speaker) {
            return false;
        } else {
            let imageName = "";
            if (speaker.token) {
                const token = game.scenes.get(speaker.scene)?.tokens?.get(speaker.token);
                if (token) {
                    imageName = token.texture.src;
                }
            }

            if (!imageName && speaker.actor) {
                const actor = Actors.instance.get(speaker.actor);
                if (actor) {
                    imageName = actor.texture.src;
                }
            }

            return imageName?.endsWith("webm") || imageName?.endsWith("mp4") || imageName?.endsWith("ogg") || false;
        }

        return false;
    });

    Handlebars.registerHelper("getBorderStyle", function (message, foundryBorder) {
        const borderOverride = game.settings.get("df-chat-cards", "borderOverride");
        if (borderOverride && shouldOverrideMessage(message)) {
            const user = game.users.get(message.user);
            return `border-color: ${user.color}`;
        }

        if (foundryBorder) {
            return `border-color: ${foundryBorder}`;
        }
        return "";
    });

    Handlebars.registerHelper("getHeaderStyle", function (message) {
        if (shouldOverrideMessage(message)) {
            const user = game.users.get(message.user);

            const cardStyle = game.settings.get("df-chat-cards", "cardStyle");
            if (cardStyle !== "header") {
                return "";
            }

            const hexColor = user.color.replace("#", "");
            var r = parseInt(hexColor.substr(0,2),16);
            var g = parseInt(hexColor.substr(2,2),16);
            var b = parseInt(hexColor.substr(4,2),16);
            var yiq = ((r*299)+(g*587)+(b*114))/1000;
            const textColor = (yiq >= 128) ? '#333' : '#E7E7E7';

            return `background-color:${user.color}; color: ${textColor};`;
        }
        return "";
    });

    Handlebars.registerHelper("getTitleStyle", function (message) {
        if (shouldOverrideMessage(message)) {
            const user = game.users.get(message.user);

            const cardStyle = game.settings.get("df-chat-cards", "cardStyle");
            if (cardStyle === "underline") {
                return `box-shadow: inset 0px -2px 0 ${user.color};`;
            } else if (cardStyle === "topBar") {
                return "";
            }
            
            const hexColor = user.color.replace("#", "");
            var r = parseInt(hexColor.substr(0,2),16);
            var g = parseInt(hexColor.substr(2,2),16);
            var b = parseInt(hexColor.substr(4,2),16);
            var yiq = ((r*299)+(g*587)+(b*114))/1000;
            const textColor = (yiq >= 128) ? '#333' : '#E7E7E7';

            return `color: ${textColor};`;
        }
        return "";
    });

    Handlebars.registerHelper("getUserColor", function (message) {
        if (shouldOverrideMessage(message)) {
            const user = game.users.get(message.user);
            return user.color;
        }
        return "";
    });

    Handlebars.registerHelper("getCardStyle", function () {
        const cardStyle = game.settings.get("df-chat-cards", "cardStyle");
        return cardStyle;
    });
});
